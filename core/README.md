# JobAid Core Service

[![Slack](https://img.shields.io/badge/Slack-JobAid%20Tech-0057B7.svg?logo=slack)](https://join.slack.com/t/jobaidtech/shared_invite/zt-15aekfxir-BrpdzPzhw3hnnqg_MEcHmA)
[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-2.1-FFDD00.svg)](https://gitlab.com/jobaid/base/-/tree/main/CODE_OF_CONDUCT.md)
[![Quality Gate Status](https://sonarcloud.io/api/project_badges/measure?project=jobaid_base&metric=alert_status)](https://sonarcloud.io/summary/new_code?id=jobaid_base)
[![License](https://img.shields.io/badge/License-MIT-0057B7.svg)](https://gitlab.com/jobaid/base/-/tree/main/LICENSE)

The core service is a self-contained service which offers the core functionality of the platform. This contains functions like CRUD operations on jobs and user profiles.

## Documentation

The core modules documentation is available at [GitLab](https://gitlab.com/jobaid/base/-/tree/main/core/documentation).

Documentation regarding the JobAid Project is available at [GitLab](https://gitlab.com/jobaid/base/-/tree/main/documentation).

After deployment the UI providing the OpenAPI documentation can be accessed under [http://localhost:8080/swagger-ui.html](http://localhost:8080/swagger-ui.html) locally.

If you create something, please take a look at the [Contribution Guidelines](https://gitlab.com/jobaid/base/-/blob/main/core/CODE_OF_CONDUCT.md) and be sure to document, what you've worked on.

## Getting Started

Start the dev environment by executing the `docker-compose.dev.yml` in the root folder.

```bash
docker-compose -f docker-compose.dev.yml up
```

By default the core service is startet using the `local` profile, which ignores security constraints.
To use the `default` profile, you'd have to provide the Google Cloud configuration, which are not available right now.
When starting the `local` profile, there will occur an exception, because the Google Cloud configuration is not available.

When developing a feature for the core module, we recommend shutting down the core service started by docker compose and instead using Gradle, as described [here](#using-gradle).
Alternatively, you can build and run your container manually following [this description](#using-docker).

### Using Docker

```bash
docker build -t jobaid/core:latest .
docker run jobaid/core -p 8080:8080
```

### Using Gradle

```bash
gradle bootRun --args='--spring.profiles.active=local'
```

The core module will be available under [http://localhost:8080/](http://localhost:8080/).

If you have any trouble with the architecture or with contributing in general, please take a look at the [Contribution Guidelines](https://gitlab.com/jobaid/base/-/blob/main/core/CODE_OF_CONDUCT.md).

## Get OpenAPI Documentation

The REST endpoints are documented in the [OpenAPI Specification](http://localhost:8080/swagger-ui.html).
It is available once you've started the backend.

## Get Involved

You're invited to join this project! Check out the [contributing guide](CONTRIBUTING.md).

If you're interested in how the project is organized at a higher level, please join our [slack](https://join.slack.com/t/jobaidtech/shared_invite/zt-15aekfxir-BrpdzPzhw3hnnqg_MEcHmA)!
