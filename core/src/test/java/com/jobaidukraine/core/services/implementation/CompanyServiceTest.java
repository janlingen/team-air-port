package com.jobaidukraine.core.services.implementation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.jobaidukraine.core.domain.Address;
import com.jobaidukraine.core.domain.Company;
import com.jobaidukraine.core.domain.Job;
import com.jobaidukraine.core.domain.User;
import com.jobaidukraine.core.services.ports.out.CompanyPort;
import java.time.LocalDateTime;
import java.util.*;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

@ExtendWith(MockitoExtension.class)
class CompanyServiceTest {

  @Mock private CompanyPort companyPort;

  @InjectMocks private CompanyService companyService;

  @Test
  void saveCompany() {
    Company company = getMockCompany();
    when(companyPort.save(company)).thenReturn(company);

    Company result = companyService.save(company);

    assertEquals(result, company);
  }

  @Test
  void findAllByPageable() {
    Page<Company> company = new PageImpl<>(List.of(getMockCompany()));
    when(companyPort.findAllByPageable(Pageable.ofSize(1))).thenReturn(company);

    Page<Company> result = companyService.findAllByPageable(PageRequest.of(0, 1));

    assertEquals(result, company);
  }

  @Test
  void findById() {
    Company company = getMockCompany();
    when(companyPort.findById(1L)).thenReturn(Optional.of(company));

    Company result = companyService.findById(1L);

    assertEquals(result, company);
  }

  @Test
  @DisplayName("no company for id 2L found")
  void findById2() {
    when(companyPort.findById(2L)).thenThrow(new NoSuchElementException());

    assertThrows(NoSuchElementException.class, () -> companyService.findById(2L));
  }

  @Test
  void update() {
    Company base = getMockCompany();
    Company update = getMockCompany();
    update.setName("Test2");
    when(companyPort.update(base)).thenReturn(update);

    Company result = companyService.update(base);

    assertEquals(result, update);
  }

  @Test
  void delete() {
    Company company = getMockCompany();

    companyService.delete(company.getId());

    verify(companyPort).delete(company.getId());
  }

  Company getMockCompany() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String name = "test";
    String url = "test-url.test";
    String email = "test@test.test";
    String logo = "image_test";
    Set<Job> jobs = new HashSet<>();
    Address address = new Address();
    User contactUser = new User();
    return new Company(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        name,
        url,
        email,
        logo,
        jobs,
        address,
        contactUser);
  }
}
