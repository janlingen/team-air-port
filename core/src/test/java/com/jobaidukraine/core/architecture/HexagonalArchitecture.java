package com.jobaidukraine.core.architecture;

import com.tngtech.archunit.core.domain.JavaClasses;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class HexagonalArchitecture extends ArchitectureElement {

  private Adapters adapters;
  private ApplicationLayer applicationLayer;
  private Services services;
  private OutgoingLayers outgoingLayers;
  private IncomingLayers incomingLayers;
  private String configurationPackage;
  private List<String> domainPackages = new ArrayList<>();

  public static HexagonalArchitecture boundedContext(String basePackage) {
    return new HexagonalArchitecture(basePackage);
  }

  public HexagonalArchitecture(String basePackage) {
    super(basePackage);
  }

  public Adapters withAdaptersLayer(String adaptersPackage) {
    this.adapters = new Adapters(this, fullQualifiedPackage(adaptersPackage));
    return this.adapters;
  }

  public HexagonalArchitecture withDomainLayer(String domainPackage) {
    this.domainPackages.add(fullQualifiedPackage(domainPackage));
    return this;
  }

  public ApplicationLayer withApplicationLayer(String applicationPackage) {
    this.applicationLayer = new ApplicationLayer(fullQualifiedPackage(applicationPackage), this);
    return this.applicationLayer;
  }

  public HexagonalArchitecture withConfiguration(String packageName) {
    this.configurationPackage = fullQualifiedPackage(packageName);
    return this;
  }

  public OutgoingLayers withOutgoingLayer(String outgoingLayerBasePackage) {
    this.outgoingLayers = new OutgoingLayers(this, fullQualifiedPackage(outgoingLayerBasePackage));
    return this.outgoingLayers;
  }

  public HexagonalArchitecture withServicesLayer(String servicesPackage) {
    this.services = new Services(this, fullQualifiedPackage(servicesPackage));
    return this;
  }

  public IncomingLayers withIncomingLayer(String incomingLayerBasePackage) {
    this.incomingLayers = new IncomingLayers(this, fullQualifiedPackage(incomingLayerBasePackage));
    return this.incomingLayers;
  }

  private void domainDoesNotDependOnOtherPackages(JavaClasses classes) {
    denyAnyDependency(
        this.domainPackages, Collections.singletonList(adapters.basePackage), classes);
    denyAnyDependency(
        this.domainPackages, Collections.singletonList(applicationLayer.basePackage), classes);
  }

  public void check(JavaClasses classes) {
    this.adapters.doesNotContainEmptyPackages();
    this.adapters.dontDependOnEachOther(classes);
    this.adapters.doesNotDependOn(this.configurationPackage, classes);

    this.applicationLayer.doesNotContainEmptyPackages();
    this.applicationLayer.doesNotDependOn(this.adapters.getBasePackage(), classes);
    this.applicationLayer.doesNotDependOn(this.configurationPackage, classes);
    this.applicationLayer.incomingAndOutgoingPortsDoNotDependOnEachOther(classes);

    this.services.doesNotContainEmptyPackages();
    this.outgoingLayers
        .getPackages()
        .forEach(outgoingLayer -> this.services.doesNotDependOn(outgoingLayer, classes));
    this.services.dontDependOnEachOther(classes);

    this.incomingLayers.doesNotContainEmptyPackages();
    this.incomingLayers.doesNotDependOn(this.services.getBasePackage(), classes);
    this.incomingLayers.dontDependOnEachOther(classes);

    this.domainDoesNotDependOnOtherPackages(classes);
  }
}
