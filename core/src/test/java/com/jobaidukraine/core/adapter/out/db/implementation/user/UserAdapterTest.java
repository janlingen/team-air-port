package com.jobaidukraine.core.adapter.out.db.implementation.user;

import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.jobaidukraine.core.adapter.out.db.entities.*;
import com.jobaidukraine.core.adapter.out.db.mapper.LanguageMapper;
import com.jobaidukraine.core.adapter.out.db.mapper.UserMapper;
import com.jobaidukraine.core.adapter.out.db.repositories.UserRepository;
import com.jobaidukraine.core.domain.*;
import java.time.LocalDateTime;
import java.util.*;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatcher;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

@ExtendWith(MockitoExtension.class)
class UserAdapterTest {

  @Mock UserRepository userRepository;
  @Mock UserMapper userMapper;
  @Mock LanguageMapper languageMapper;
  @InjectMocks UserAdapter userAdapter;

  @Test
  void findExistingUserById() {
    UserEntity userEntity = getUserEntityWithId(1L);
    User user = getUserWithId(1L);

    when(userRepository.findById(1L)).thenReturn(Optional.of(userEntity));
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity)))).thenReturn(user);

    Optional<User> result = userAdapter.findById(1L);

    Assertions.assertThat(result).isPresent();
    Assertions.assertThat(result.get()).usingRecursiveComparison().isEqualTo(user);
  }

  @Test
  void findAllExistingUsersByPageable() {
    UserEntity userEntity1 = getUserEntityWithId(1L);
    UserEntity userEntity2 = getUserEntityWithId(2L);
    User user1 = getUserWithId(1L);
    User user2 = getUserWithId(2L);
    Page<UserEntity> userEntityPage = new PageImpl<>(List.of(userEntity1, userEntity2));

    when(userRepository.findAll(PageRequest.of(0, 10))).thenReturn(userEntityPage);
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity1)))).thenReturn(user1);
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity2)))).thenReturn(user2);

    Page<User> result = userAdapter.findAllByPageable(PageRequest.of(0, 10));

    Assertions.assertThat(result.getTotalElements()).isEqualTo(2);
    Assertions.assertThat(result.getContent())
        .usingRecursiveComparison()
        .isEqualTo(List.of(user1, user2));
  }

  @Test
  void findExistingUsersByEmail() {
    UserEntity userEntity = getUserEntityWithId(1L);
    User user = getUserWithId(1L);

    when(userRepository.findByEmail(userEntity.getEmail())).thenReturn(Optional.of(userEntity));
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity)))).thenReturn(user);

    Optional<User> result = userAdapter.findByEmail(userEntity.getEmail());

    Assertions.assertThat(result).isPresent();
    Assertions.assertThat(result.get()).usingRecursiveComparison().isEqualTo(user);
  }

  @Test
  void saveUser() {
    User user = getUserWithId(1L);
    User userNull = getUserWithId(null);
    UserEntity userEntity = getUserEntityWithId(1L);
    UserEntity userEntityNull = getUserEntityWithId(null);

    when(userMapper.toEntity(argThat(new UserMatcher(userNull)))).thenReturn(userEntityNull);
    when(userRepository.save(argThat(new UserEntityMatcher(userEntityNull))))
        .thenReturn(userEntity);
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity)))).thenReturn(user);

    User result = userAdapter.save(userNull);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(user);
  }

  @Test
  void updateExistingUser() {
    UserEntity userEntity = getUserEntityWithId(1L);
    User user = getUserWithId(1L);

    when(userRepository.findById(1L)).thenReturn(Optional.of(userEntity));
    when(userRepository.save(argThat(new UserEntityMatcher(userEntity)))).thenReturn(userEntity);
    when(userMapper.toDomain(argThat(new UserEntityMatcher(userEntity)))).thenReturn(user);

    User result = userAdapter.update(user);

    Assertions.assertThat(result).usingRecursiveComparison().isEqualTo(user);
  }

  @Test
  void deleteExistingUser() {
    UserEntity userEntity = getUserEntityWithId(1L);

    when(userRepository.findById(1L)).thenReturn(Optional.of(userEntity));

    userAdapter.delete(1L);

    verify(userRepository).save(argThat(new UserEntityMatcher(userEntity)));
  }

  User getUserWithId(Long id) {
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String email = "john.doe@foo.de";
    String firstName = "John";
    String lastName = "Doe";
    String position = "Developer";
    String country = "DE";
    Role role = Role.USER;
    Experience experience = Experience.PROFESSIONAL;
    Set<LanguageLevel> languageLevels =
        new HashSet<>(Set.of(new LanguageLevel("GERMAN", LanguageSkill.BASIC)));
    LocalDateTime lastDigestSent = LocalDateTime.of(2020, 12, 24, 11, 10);
    Integer digestPeriodMinutes = 5;
    Set<Search> searches = new HashSet<>();

    return new User(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        email,
        firstName,
        lastName,
        position,
        country,
        role,
        experience,
        languageLevels,
        lastDigestSent,
        digestPeriodMinutes,
        searches);
  }

  UserEntity getUserEntityWithId(Long id) {
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String email = "john.doe@foo.de";
    String firstName = "John";
    String lastName = "Doe";
    String position = "Developer";
    String country = "DE";
    RoleEntity role = RoleEntity.USER;
    ExperienceEntity experience = ExperienceEntity.PROFESSIONAL;
    Set<LanguageLevelEntity> languageLevelEntities =
        new HashSet<>(Set.of(new LanguageLevelEntity("GERMAN", LanguageSkillEntity.BASIC)));
    LocalDateTime lastDigestSent = LocalDateTime.of(2020, 12, 24, 11, 10);
    Integer digestPeriodMinutes = 5;
    Set<SearchEntity> searches = new HashSet<>();

    return new UserEntity(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        email,
        firstName,
        lastName,
        position,
        country,
        role,
        experience,
        languageLevelEntities,
        lastDigestSent,
        digestPeriodMinutes,
        searches);
  }

  private static class UserMatcher implements ArgumentMatcher<User> {
    private final User expected;

    public UserMatcher(User expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(User actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      return Objects.equals(expected.getId(), actual.getId())
          && Objects.equals(expected.getCreatedAt(), actual.getCreatedAt())
          && Objects.equals(expected.getUpdatedAt(), actual.getUpdatedAt())
          && Objects.equals(expected.getVersion(), actual.getVersion())
          && Objects.equals(expected.getDeleted(), actual.getDeleted())
          && Objects.equals(expected.getEmail(), actual.getEmail())
          && Objects.equals(expected.getFirstname(), actual.getFirstname())
          && Objects.equals(expected.getLastname(), actual.getLastname())
          && Objects.equals(expected.getPosition(), actual.getPosition())
          && Objects.equals(expected.getCountry(), actual.getCountry())
          && Objects.equals(expected.getRole(), actual.getRole())
          && Objects.equals(expected.getExperience(), actual.getExperience())
          && Objects.equals(expected.getLanguageLevels(), actual.getLanguageLevels())
          && Objects.equals(expected.getLastDigestSent(), actual.getLastDigestSent())
          && Objects.equals(expected.getDigestPeriodMinutes(), actual.getDigestPeriodMinutes());
    }
  }

  private static class UserEntityMatcher implements ArgumentMatcher<UserEntity> {
    private final UserEntity expected;

    public UserEntityMatcher(UserEntity expected) {
      this.expected = expected;
    }

    @Override
    public boolean matches(UserEntity actual) {
      if (expected == null || actual == null) {
        return expected == null && actual == null;
      }

      return Objects.equals(expected.getId(), actual.getId())
          && Objects.equals(expected.getCreatedAt(), actual.getCreatedAt())
          && Objects.equals(expected.getUpdatedAt(), actual.getUpdatedAt())
          && Objects.equals(expected.getVersion(), actual.getVersion())
          && Objects.equals(expected.getDeleted(), actual.getDeleted())
          && Objects.equals(expected.getEmail(), actual.getEmail())
          && Objects.equals(expected.getFirstname(), actual.getFirstname())
          && Objects.equals(expected.getLastname(), actual.getLastname())
          && Objects.equals(expected.getPosition(), actual.getPosition())
          && Objects.equals(expected.getCountry(), actual.getCountry())
          && Objects.equals(expected.getRole(), actual.getRole())
          && Objects.equals(expected.getExperience(), actual.getExperience())
          && Objects.equals(expected.getLanguageLevels(), actual.getLanguageLevels())
          && Objects.equals(expected.getLastDigestSent(), actual.getLastDigestSent())
          && Objects.equals(expected.getDigestPeriodMinutes(), actual.getDigestPeriodMinutes());
    }
  }
}
