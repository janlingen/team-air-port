package com.jobaidukraine.core.adapter.in.rest.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobaidukraine.core.adapter.in.rest.assemblers.UserResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.*;
import com.jobaidukraine.core.adapter.in.rest.mapper.UserMapper;
import com.jobaidukraine.core.domain.*;
import com.jobaidukraine.core.services.ports.in.queries.UserQuery;
import com.jobaidukraine.core.services.ports.in.usecases.UserUseCase;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(UserController.class)
@AutoConfigureMockMvc(addFilters = false)
@WithMockUser(
    username = "Picard",
    roles = {"ADMIN"})
@ActiveProfiles(profiles = "local")
class UserControllerTest {

  @Autowired MockMvc mock;

  @MockBean UserQuery userQuery;
  @MockBean UserUseCase userUseCase;
  @MockBean PagedResourcesAssembler<UserDto> pagedResourcesAssembler;
  @MockBean UserMapper userMapper;
  @MockBean UserResourceAssembler userResourceAssembler;

  static final String USER_BASE_URL = "/v1.0/users";
  static final DateTimeFormatter TIMESTAMP_FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

  @Test
  void createUserFromJson() throws Exception {
    User user = getMockUser();
    UserDto userDto = getMockUserDto();

    when(userMapper.dtoToDomain(any(UserDto.class))).thenReturn(user);
    when(userUseCase.save(any(User.class))).thenReturn(user);
    when(userMapper.domainToDto(any(User.class))).thenReturn(userDto);
    when(userResourceAssembler.toModel(any(UserDto.class))).thenReturn(EntityModel.of(userDto));

    mock.perform(
            post(USER_BASE_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getUserJson()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(userDto.getId()))
        .andExpect(
            jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(userDto.getCreatedAt())))
        .andExpect(
            jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(userDto.getUpdatedAt())))
        .andExpect(jsonPath("$.version").value(userDto.getVersion()))
        .andExpect(jsonPath("$.deleted").value(userDto.getDeleted()))
        .andExpect(jsonPath("$.email").value(userDto.getEmail()))
        .andExpect(jsonPath("$.firstname").value(userDto.getFirstname()))
        .andExpect(jsonPath("$.lastname").value(userDto.getLastname()))
        .andExpect(jsonPath("$.position").value(userDto.getPosition()))
        .andExpect(jsonPath("$.country").value(userDto.getCountry()))
        .andExpect(jsonPath("$.role").value(userDto.getRole().name()))
        .andExpect(jsonPath("$.experience").value(userDto.getExperience().name()))
        .andExpect(jsonPath("$.languageLevels").isArray());
  }

  @Test
  void readPagableListOfUsers() throws Exception {
    User user = getMockUser();
    User user2 = getMockUser();
    user2.setId(2L);
    Page<User> userPage = new PageImpl<>(List.of(user, user2));
    UserDto userDto = getMockUserDto();
    UserDto userDto2 = getMockUserDto();
    userDto2.setId(2L);

    PagedModel<EntityModel<UserDto>> pagedModel =
        PagedModel.wrap(List.of(userDto, userDto2), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(userQuery.findAllByPageable(any(Pageable.class))).thenReturn(userPage);
    when(userMapper.domainToDto(any(User.class))).thenReturn(userDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(UserResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(USER_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.userDtoes").isArray())
        .andExpect(jsonPath("$._embedded.userDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.userDtoes[0].id").value(userDto.getId()))
        .andExpect(jsonPath("$._embedded.userDtoes[1].id").value(userDto2.getId()));
  }

  @Test
  void readPagableListOfOneUser() throws Exception {
    User user = getMockUser();
    Page<User> userPage = new PageImpl<>(List.of(user));
    UserDto userDto = getMockUserDto();
    PagedModel<EntityModel<UserDto>> pagedModel =
        PagedModel.wrap(List.of(userDto), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(userQuery.findAllByPageable(any(Pageable.class))).thenReturn(userPage);
    when(userMapper.domainToDto(any(User.class))).thenReturn(userDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(UserResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(USER_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.userDtoes[0].id").value(userDto.getId()))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].createdAt")
                .value(TIMESTAMP_FORMATTER.format(userDto.getCreatedAt())))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].updatedAt")
                .value(TIMESTAMP_FORMATTER.format(userDto.getUpdatedAt())))
        .andExpect(jsonPath("$._embedded.userDtoes[0].version").value(userDto.getVersion()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].deleted").value(userDto.getDeleted()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].email").value(userDto.getEmail()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].firstname").value(userDto.getFirstname()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].lastname").value(userDto.getLastname()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].position").value(userDto.getPosition()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].country").value(userDto.getCountry()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].role").value(userDto.getRole().name()))
        .andExpect(
            jsonPath("$._embedded.userDtoes[0].experience").value(userDto.getExperience().name()))
        .andExpect(jsonPath("$._embedded.userDtoes[0].languageLevels").isArray());
  }

  @Test
  void readPageableListOfNoUser() throws Exception {
    Page<User> userPages = new PageImpl<>(Collections.emptyList());

    when(userQuery.findAllByPageable(any(Pageable.class))).thenReturn(userPages);

    mock.perform(get(USER_BASE_URL).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.userDtoes").doesNotExist());
  }

  @Test
  void readUser() throws Exception {
    User user = getMockUser();
    UserDto userDto = getMockUserDto();
    EntityModel<UserDto> entityModel = EntityModel.of(userDto);

    when(userQuery.findById(1L)).thenReturn(user);
    when(userMapper.domainToDto(any(User.class))).thenReturn(userDto);
    when(userResourceAssembler.toModel(any(UserDto.class))).thenReturn(entityModel);

    mock.perform(get(USER_BASE_URL + "/{id}", 1L).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(userDto.getId()))
        .andExpect(
            jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(userDto.getCreatedAt())))
        .andExpect(
            jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(userDto.getUpdatedAt())))
        .andExpect(jsonPath("$.version").value(userDto.getVersion()))
        .andExpect(jsonPath("$.deleted").value(userDto.getDeleted()))
        .andExpect(jsonPath("$.email").value(userDto.getEmail()))
        .andExpect(jsonPath("$.firstname").value(userDto.getFirstname()))
        .andExpect(jsonPath("$.lastname").value(userDto.getLastname()))
        .andExpect(jsonPath("$.position").value(userDto.getPosition()))
        .andExpect(jsonPath("$.country").value(userDto.getCountry()))
        .andExpect(jsonPath("$.role").value(userDto.getRole().name()))
        .andExpect(jsonPath("$.experience").value(userDto.getExperience().name()))
        .andExpect(jsonPath("$.languageLevels").isArray());
  }

  @Test
  void updateUserFromJson() throws Exception {
    User user = getMockUser();
    UserDto userDto = getMockUserDto();
    EntityModel<UserDto> entityModel = EntityModel.of(userDto);

    when(userMapper.dtoToDomain(any(UserDto.class))).thenReturn(user);
    when(userUseCase.update(any(User.class))).thenReturn(user);
    when(userMapper.domainToDto(any(User.class))).thenReturn(userDto);
    when(userResourceAssembler.toModel(any(UserDto.class))).thenReturn(entityModel);

    mock.perform(
            patch(USER_BASE_URL + "/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getUserJson()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(userDto.getId()))
        .andExpect(
            jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(userDto.getCreatedAt())))
        .andExpect(
            jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(userDto.getUpdatedAt())))
        .andExpect(jsonPath("$.version").value(userDto.getVersion()))
        .andExpect(jsonPath("$.deleted").value(userDto.getDeleted()))
        .andExpect(jsonPath("$.email").value(userDto.getEmail()))
        .andExpect(jsonPath("$.firstname").value(userDto.getFirstname()))
        .andExpect(jsonPath("$.lastname").value(userDto.getLastname()))
        .andExpect(jsonPath("$.position").value(userDto.getPosition()))
        .andExpect(jsonPath("$.country").value(userDto.getCountry()))
        .andExpect(jsonPath("$.role").value(userDto.getRole().name()))
        .andExpect(jsonPath("$.experience").value(userDto.getExperience().name()))
        .andExpect(jsonPath("$.languageLevels").isArray());
  }

  @Test
  void deleteUser() throws Exception {
    mock.perform(
            delete(USER_BASE_URL + "/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getUserJson()))
        .andExpect(status().is(204));

    verify(userUseCase, times(1)).delete(1L);
  }

  User getMockUser() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    Integer version = 1;
    Boolean deleted = false;
    String email = "user@gmail.com";
    String firstname = "John";
    String lastname = "Doe";
    String position = "Developer";
    String country = "France";
    Role role = Role.USER;
    Experience experience = Experience.STUDENT;
    Set<LanguageLevel> languageLevels = new HashSet<>();
    LocalDateTime lastDigestSent = LocalDateTime.of(2020, 12, 24, 11, 10);
    Integer digestPeriodMinutes = 5;
    Set<Search> searches = new HashSet<>();
    return new User(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        email,
        firstname,
        lastname,
        position,
        country,
        role,
        experience,
        languageLevels,
        lastDigestSent,
        digestPeriodMinutes,
        searches);
  }

  UserDto getMockUserDto() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    Integer version = 1;
    Boolean deleted = false;
    String email = "user@gmail.com";
    String firstname = "John";
    String lastname = "Doe";
    String position = "Developer";
    String country = "France";
    RoleDto role = RoleDto.USER;
    ExperienceDto experience = ExperienceDto.STUDENT;
    Set<LanguageLevelDto> languageLevels = new HashSet<>();
    LocalDateTime lastDigestSent = LocalDateTime.of(2020, 12, 24, 11, 10);
    Integer digestPeriodMinutes = 5;
    Set<SearchDto> searches = new HashSet<>();
    return new UserDto(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        email,
        firstname,
        lastname,
        position,
        country,
        role,
        experience,
        languageLevels,
        lastDigestSent,
        digestPeriodMinutes,
        searches);
  }

  String getUserJson() throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(getMockUserDto());
  }
}
