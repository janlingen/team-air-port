package com.jobaidukraine.core.adapter.in.rest.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jobaidukraine.core.adapter.in.rest.assemblers.CompanyResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.AddressDto;
import com.jobaidukraine.core.adapter.in.rest.dto.CompanyDto;
import com.jobaidukraine.core.adapter.in.rest.dto.JobDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.AddressMapper;
import com.jobaidukraine.core.adapter.in.rest.mapper.CompanyMapper;
import com.jobaidukraine.core.domain.Address;
import com.jobaidukraine.core.domain.Company;
import com.jobaidukraine.core.domain.Job;
import com.jobaidukraine.core.domain.User;
import com.jobaidukraine.core.services.implementation.UserService;
import com.jobaidukraine.core.services.ports.in.queries.CompanyQuery;
import com.jobaidukraine.core.services.ports.in.usecases.CompanyUseCase;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@AutoConfigureMockMvc
@WebMvcTest(CompanyController.class)
@WithMockUser(
    username = "Skywalker",
    roles = {"ADMIN"})
@ActiveProfiles(profiles = "local")
class CompanyControllerTest {

  @Autowired private MockMvc mock;

  @MockBean private UserService userService;
  @MockBean private CompanyUseCase companyUseCase;
  @MockBean private CompanyQuery companyQuery;
  @MockBean private PagedResourcesAssembler<CompanyDto> pagedResourcesAssembler;
  @MockBean private CompanyResourceAssembler companyResourceAssembler;
  @MockBean private CompanyMapper companyMapper;
  @MockBean private AddressMapper addressMapper;

  static final String COMPANY_BASE_URL = "/v1.0/companies";
  static final DateTimeFormatter TIMESTAMP_FORMATTER =
      DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");

  @Test
  void createCompanyFromJson() throws Exception {
    Company company = getMockCompany();
    CompanyDto companyDto = getMockCompanyDto();

    when(companyMapper.dtoToDomain(any(CompanyDto.class))).thenReturn(company);
    when(companyUseCase.save(any(Company.class))).thenReturn(company);
    when(companyMapper.domainToDto(any(Company.class))).thenReturn(companyDto);
    when(companyResourceAssembler.toModel(any(CompanyDto.class)))
        .thenReturn(EntityModel.of(companyDto));

    mock.perform(
            post(COMPANY_BASE_URL)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getCompanyJson()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(companyDto.getId()))
        .andExpect(
            jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(companyDto.getCreatedAt())))
        .andExpect(
            jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(companyDto.getUpdatedAt())))
        .andExpect(jsonPath("$.version").value(companyDto.getVersion()))
        .andExpect(jsonPath("$.deleted").value(companyDto.getDeleted()))
        .andExpect(jsonPath("$.name").value(companyDto.getName()))
        .andExpect(jsonPath("$.url").value(companyDto.getUrl()))
        .andExpect(jsonPath("$.jobs").isArray())
        .andExpect(jsonPath("$.address").value(companyDto.getAddress()));
  }

  @Test
  void readPageableListOfOneCompany() throws Exception {
    Company company = getMockCompany();
    CompanyDto companyDto = getMockCompanyDto();
    Page<Company> companyPage = new PageImpl<>(List.of(company));
    PagedModel<EntityModel<CompanyDto>> pagedModel =
        PagedModel.wrap(List.of(companyDto), new PagedModel.PageMetadata(1, 1, 1, 1));

    when(companyQuery.findAllByPageable(any(Pageable.class))).thenReturn(companyPage);
    when(companyMapper.domainToDto(any(Company.class))).thenReturn(companyDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(CompanyResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(COMPANY_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.companyDtoes.[0].id").value(companyDto.getId()))
        .andExpect(
            jsonPath("$._embedded.companyDtoes.[0].createdAt")
                .value(TIMESTAMP_FORMATTER.format(companyDto.getCreatedAt())))
        .andExpect(
            jsonPath("$._embedded.companyDtoes.[0].updatedAt")
                .value(TIMESTAMP_FORMATTER.format(companyDto.getUpdatedAt())))
        .andExpect(jsonPath("$._embedded.companyDtoes.[0].version").value(companyDto.getVersion()))
        .andExpect(jsonPath("$._embedded.companyDtoes.[0].deleted").value(companyDto.getDeleted()))
        .andExpect(jsonPath("$._embedded.companyDtoes.[0].name").value(companyDto.getName()))
        .andExpect(jsonPath("$._embedded.companyDtoes.[0].url").value(companyDto.getUrl()))
        .andExpect(jsonPath("$._embedded.companyDtoes.[0].jobs").isArray())
        .andExpect(jsonPath("$._embedded.companyDtoes.[0].address").value(companyDto.getAddress()));
  }

  @Test
  void readPageableListOfCompanies() throws Exception {
    Company company = getMockCompany();
    Page<Company> companyPage = new PageImpl<>(List.of(company));
    CompanyDto companyDto = getMockCompanyDto();
    CompanyDto companyDto2 = getMockCompanyDto();
    companyDto2.setId(2L);
    PagedModel<EntityModel<CompanyDto>> pagedModel =
        PagedModel.wrap(List.of(companyDto, companyDto2), new PagedModel.PageMetadata(2, 2, 2, 2));

    when(companyQuery.findAllByPageable(any(Pageable.class))).thenReturn(companyPage);
    when(companyMapper.domainToDto(any(Company.class))).thenReturn(companyDto);
    when(pagedResourcesAssembler.toModel(any(Page.class), any(CompanyResourceAssembler.class)))
        .thenReturn(pagedModel);

    mock.perform(get(COMPANY_BASE_URL))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.companyDtoes").isArray())
        .andExpect(jsonPath("$._embedded.companyDtoes").isNotEmpty())
        .andExpect(jsonPath("$._embedded.companyDtoes.[0].id").value(companyDto.getId()))
        .andExpect(jsonPath("$._embedded.companyDtoes.[1].id").value(companyDto2.getId()));
  }

  @Test
  void readPageableListOfNoCompany() throws Exception {
    Page<Company> companyPages = new PageImpl<>(Collections.emptyList());

    when(companyQuery.findAllByPageable(any(Pageable.class))).thenReturn(companyPages);

    mock.perform(get(COMPANY_BASE_URL).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$._embedded.companyDtoes").doesNotExist());
  }

  @Test
  void readCompany() throws Exception {
    Company company = getMockCompany();
    CompanyDto companyDto = getMockCompanyDto();
    EntityModel<CompanyDto> model = EntityModel.of(companyDto);

    when(companyQuery.findById(1L)).thenReturn(company);
    when(companyMapper.domainToDto(any(Company.class))).thenReturn(companyDto);
    when(companyResourceAssembler.toModel(any(CompanyDto.class))).thenReturn(model);

    mock.perform(get(COMPANY_BASE_URL + "/{id}", 1L).accept(MediaType.APPLICATION_JSON))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(companyDto.getId()))
        .andExpect(
            jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(companyDto.getCreatedAt())))
        .andExpect(
            jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(companyDto.getUpdatedAt())))
        .andExpect(jsonPath("$.version").value(companyDto.getVersion()))
        .andExpect(jsonPath("$.deleted").value(companyDto.getDeleted()))
        .andExpect(jsonPath("$.name").value(companyDto.getName()))
        .andExpect(jsonPath("$.url").value(companyDto.getUrl()))
        .andExpect(jsonPath("$.jobs").isArray())
        .andExpect(jsonPath("$.address").value(companyDto.getAddress()));
  }

  @Test
  void updateCompanyFromJson() throws Exception {
    Company company = getMockCompany();
    CompanyDto companyDto = getMockCompanyDto();
    EntityModel<CompanyDto> model = EntityModel.of(companyDto);

    when(companyMapper.dtoToDomain(any(CompanyDto.class))).thenReturn(company);
    when(companyUseCase.update(any(Company.class))).thenReturn(company);
    when(companyMapper.domainToDto(any(Company.class))).thenReturn(companyDto);
    when(companyResourceAssembler.toModel(any(CompanyDto.class))).thenReturn(model);

    mock.perform(
            patch(COMPANY_BASE_URL + "/{id}", 2)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getCompanyJson()))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.id").value(companyDto.getId()))
        .andExpect(
            jsonPath("$.createdAt").value(TIMESTAMP_FORMATTER.format(companyDto.getCreatedAt())))
        .andExpect(
            jsonPath("$.updatedAt").value(TIMESTAMP_FORMATTER.format(companyDto.getUpdatedAt())))
        .andExpect(jsonPath("$.version").value(companyDto.getVersion()))
        .andExpect(jsonPath("$.deleted").value(companyDto.getDeleted()))
        .andExpect(jsonPath("$.name").value(companyDto.getName()))
        .andExpect(jsonPath("$.url").value(companyDto.getUrl()))
        .andExpect(jsonPath("$.jobs").isArray())
        .andExpect(jsonPath("$.address").value(companyDto.getAddress()));
  }

  @Test
  void deleteCompany() throws Exception {
    mock.perform(
            delete(COMPANY_BASE_URL + "/{id}", 1)
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content(getCompanyJson()))
        .andExpect(status().is(204));

    verify(companyUseCase, times(1)).delete(1L);
  }

  Company getMockCompany() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String name = "test";
    String url = "test-url.test";
    String email = "test@test.test";
    String logo = "image_test";
    Set<Job> jobs = new HashSet<>();
    Address address = new Address();
    User contactUser = new User();
    return new Company(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        name,
        url,
        email,
        logo,
        jobs,
        address,
        contactUser);
  }

  CompanyDto getMockCompanyDto() {
    Long id = 1L;
    LocalDateTime createdAt = LocalDateTime.of(2021, 12, 24, 11, 10);
    LocalDateTime updatedAt = LocalDateTime.of(2021, 12, 24, 12, 10);
    Integer version = 1;
    Boolean deleted = false;
    String name = "test";
    String url = "test-url.test";
    String email = "test@test.test";
    String logo = "image_test";
    Set<JobDto> jobs = new HashSet<>();
    AddressDto address = new AddressDto();
    User contactUser = new User();
    return new CompanyDto(
        id,
        createdAt,
        updatedAt,
        version,
        deleted,
        name,
        url,
        email,
        logo,
        jobs,
        address,
        contactUser);
  }

  String getCompanyJson() throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(getMockCompanyDto());
  }
}
