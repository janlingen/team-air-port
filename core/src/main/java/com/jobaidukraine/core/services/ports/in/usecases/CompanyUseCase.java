package com.jobaidukraine.core.services.ports.in.usecases;

import com.jobaidukraine.core.domain.Company;

public interface CompanyUseCase {
  Company save(Company company);

  Company update(Company company);

  void delete(long id);
}
