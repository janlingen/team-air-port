package com.jobaidukraine.core.domain;

import java.time.LocalDateTime;
import java.util.Set;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class User {
  private Long id;
  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;
  private Integer version;
  private Boolean deleted;

  private String email;
  private String firstname;
  private String lastname;
  private String position;
  private String country;

  private Role role;
  private Experience experience;
  private Set<LanguageLevel> languageLevels;

  private LocalDateTime lastDigestSent;
  private Integer digestPeriodMinutes;

  Set<Search> searches;
}
