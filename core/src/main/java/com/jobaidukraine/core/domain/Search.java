package com.jobaidukraine.core.domain;

import java.util.List;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Search {
  private Float latitude;
  private Float longitude;
  private Float radius;
  private String buzzwords;
}
