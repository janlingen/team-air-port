package com.jobaidukraine.core.domain;

import java.time.LocalDateTime;
import lombok.Data;

@Data
public class Housing {
  private Long id;
  private LocalDateTime createdAt;
  private LocalDateTime updatedAt;
  private Integer version;

  private String title;
  private String description;
  private Integer capacity;

  private Address address;
}
