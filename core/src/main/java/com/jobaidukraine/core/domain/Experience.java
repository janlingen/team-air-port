package com.jobaidukraine.core.domain;

public enum Experience {
  PROFESSIONAL("PROFESSIONAL"),
  NEW_GRADUATE("NEW_GRADUATE"),
  STUDENT("STUDENT");

  final String experienceName;

  Experience(String experience) {
    this.experienceName = experience;
  }

  public String getExperience() {
    return experienceName;
  }
}
