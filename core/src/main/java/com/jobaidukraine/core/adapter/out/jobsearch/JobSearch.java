package com.jobaidukraine.core.adapter.out.jobsearch;

import com.jobaidukraine.core.domain.Classification;
import com.jobaidukraine.core.domain.LanguageLevel;
import com.jobaidukraine.core.domain.LanguageSkill;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Service;

@Service
public class JobSearch {

  public List<JobSearchResult> searchJob(
      LocalDateTime changedSince,
      Float latitude,
      Float longitude,
      Float radius,
      List<String> buzzWords,
      List<Classification> qualifications,
      Set<LanguageLevel> languageLevels) {

    //TODO: Use the Jobsucher query to find suitable jobs for the search
    return Collections.emptyList();
  }
}
