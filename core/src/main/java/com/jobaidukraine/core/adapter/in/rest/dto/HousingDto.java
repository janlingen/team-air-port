package com.jobaidukraine.core.adapter.in.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class HousingDto {
  @Schema(
      description =
          "The database generated ID of the housing. "
              + "This has to be null when creating a new housing. "
              + "This has to be set when updating an existing housing.")
  private Long id;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(
      description =
          "The database generated creation date of the housing. "
              + "This has to be null when creating a new housing.",
      example = "2020-01-01T00:00:00.000Z")
  private LocalDateTime createdAt;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(
      description =
          "The database generated last update date of the housing. "
              + "This has to be null when creating a new housing.",
      example = "2020-01-01T12:00:00.000Z")
  private LocalDateTime updatedAt;

  @Schema(
      description =
          "The database generated version of the housing. "
              + "This has to be null when creating a new housing.",
      example = "1")
  private Integer version;

  @Schema(description = "The name of the housing.", example = "Friedenshaus")
  private String title;

  @Schema(description = "The description of the housing.", example = "A nice place to live.")
  private String description;

  @Min(value = 1, message = "The capacity of the housing has to be at least 1.")
  @Schema(description = "The capacity of the housing.", example = "4")
  private Integer capacity;

  @Valid
  @NotNull(message = "The address may not be null.")
  @NotEmpty(message = "The address may not be empty.")
  @Schema(description = "The address of the housing.")
  private AddressDto address;
}
