package com.jobaidukraine.core.adapter.out.digest;

import com.jobaidukraine.core.adapter.out.emailsender.EmailSenderService;
import com.jobaidukraine.core.adapter.out.jobsearch.JobSearch;
import com.jobaidukraine.core.adapter.out.jobsearch.JobSearchResult;
import com.jobaidukraine.core.domain.Job;
import com.jobaidukraine.core.domain.Search;
import com.jobaidukraine.core.domain.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
public class DigestService {

    EmailSenderService emailService;

    JobSearch jobSearch;

    public void sendUpdates(){
        LocalDateTime currentTime = LocalDateTime.now();
        List<User> users = findUsersWithMatchinDigestPeriod(currentTime);

        for(User currentUser : users) {
            if (currentUser.getEmail() != null) {
                List<JobSearchResult> newOrUpdatedJobs = new ArrayList<>();

                for (Search currentSearch : currentUser.getSearches()) {
                    newOrUpdatedJobs.addAll(jobSearch.searchJob(currentUser.getLastDigestSent(),
                            currentSearch.getLatitude(),
                            currentSearch.getLongitude(),
                            currentSearch.getRadius(),
                            Arrays.asList((currentSearch.getBuzzwords().split(","))),
                            Collections.emptyList(),
                            currentUser.getLanguageLevels()));
                }

                if(!newOrUpdatedJobs.isEmpty()) {
                    currentTime = LocalDateTime.now();
                    boolean successful = emailService.sendMail(currentUser.getEmail(), "New jobs since " + currentUser.getLastDigestSent().toString() + " from JobAidUkraine",
                            createMailBody(newOrUpdatedJobs));
                    if (successful){
                        currentUser.setLastDigestSent(currentTime);
                    }
                }
            }
        }

    }

    public List<User> findUsersWithMatchinDigestPeriod(LocalDateTime currentTime) {
        //TODO: add a suitable query finding all Users, where lastDigestTime + DigestPeriod > currentTime
        return Collections.emptyList();
    }

    public String createMailBody(List<JobSearchResult> newOrUpdatedJobs) {
        List<JobSearchResult> sortedJobsResults = newOrUpdatedJobs.stream().sorted((j1, j2) -> Integer.compare(j2.getRating(), j1.getRating())).collect(Collectors.toList());
        StringBuilder body = new StringBuilder();

        //TODO: nicely format the email
        body.append("Here are some new job offers to your searches: \n\n");

        for (JobSearchResult sortedJob : sortedJobsResults) {
            Job currentJob = sortedJob.getJob();
            body.append("Job title: ").append(sortedJob.getJob().getTitle()).append("\n")
                    .append("Description: ").append(currentJob.getDescription()).append("\n")
                    .append("Rating: ").append(sortedJob.getRating()).append("\n\n");
        }
        return body.toString();
    }

}
