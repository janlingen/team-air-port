package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.JobEntity;
import com.jobaidukraine.core.domain.Job;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "OutDbJobMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {AddressMapper.class, LanguageMapper.class})
public interface JobMapper {
  Job toDomain(JobEntity jobEntity);

  JobEntity toEntity(Job job);
}
