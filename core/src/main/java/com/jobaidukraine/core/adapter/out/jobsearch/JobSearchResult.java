package com.jobaidukraine.core.adapter.out.jobsearch;

import com.jobaidukraine.core.domain.Job;
import lombok.Data;

@Data
public class JobSearchResult {
  private Job job;
  private Integer rating;
}
