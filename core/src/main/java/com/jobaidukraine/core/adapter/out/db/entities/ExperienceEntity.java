package com.jobaidukraine.core.adapter.out.db.entities;

public enum ExperienceEntity {
  PROFESSIONAL,
  NEW_GRADUATE,
  STUDENT
}
