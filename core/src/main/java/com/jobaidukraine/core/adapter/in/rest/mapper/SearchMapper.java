package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.SearchDto;
import com.jobaidukraine.core.domain.Search;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring", implementationName = "InRestSearchMapperImpl")
public interface SearchMapper {
    Search dtoToDomain(SearchDto searchDto);

    SearchDto domainToDto(Search search);
}
