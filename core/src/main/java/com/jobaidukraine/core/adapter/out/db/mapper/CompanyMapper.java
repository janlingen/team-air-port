package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.CompanyEntity;
import com.jobaidukraine.core.domain.Company;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "OutDbCompanyMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {JobMapper.class, AddressMapper.class, UserMapper.class})
public interface CompanyMapper {
  Company toDomain(CompanyEntity companyEntity);

  CompanyEntity toEntity(Company company);
}
