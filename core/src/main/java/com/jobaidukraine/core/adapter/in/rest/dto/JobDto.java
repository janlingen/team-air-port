package com.jobaidukraine.core.adapter.in.rest.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import java.util.Set;
import javax.validation.Valid;
import javax.validation.constraints.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class JobDto {
  @Schema(
      description =
          "The database generated job ID. "
              + "This hast to be null when creating a new job. "
              + "This has to be set when updating an existing job.",
      example = "1")
  private Long id;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(
      description =
          "The database generated creation date of the job. "
              + "This has to be null when creating a new job.",
      example = "2020-01-01T00:00:00.000Z")
  private LocalDateTime createdAt;

  @JsonDeserialize(using = LocalDateTimeDeserializer.class)
  @JsonSerialize(using = LocalDateTimeSerializer.class)
  @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'")
  @Schema(
      description =
          "The database generated last update date of the job. "
              + "This has to be null when creating a new job.",
      example = "2020-01-01T12:00:00.000Z")
  private LocalDateTime updatedAt;

  @Schema(
      description =
          "The database generated version of the job. "
              + "This has to be null when creating a new job.",
      example = "1")
  private Integer version;

  private Boolean deleted;

  @NotNull(message = "The job title may not be null.")
  @NotEmpty(message = "The job title may not be empty.")
  @Schema(description = "The job title.", required = true, example = "Software Engineer")
  private String title;

  @Schema(
      description = "The job description.",
      example = "We are looking for a Software Engineer to join our team.")
  private String description;

  @Schema(description = "The job type.", example = "Full-time")
  private String jobType;

  @Pattern(
      regexp =
          "^https?:\\/\\/(?:www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b(?:[-a-zA-Z0-9()@:%_\\+.~#?&\\/=]*)$",
      message = "The application url is not valid.")
  @Schema(
      description = "The application link for the job offer",
      example = "https://www.company.com/apply/1")
  private String applicationLink;

  @Email(message = "The email is not valid.")
  @Schema(description = " The application mail for the job offer", example = "apply@company.com")
  private String applicationMail;

  @Pattern(
      regexp =
          "^https?:\\/\\/(?:www\\.)?[-a-zA-Z0-9@:%._\\+~#=]{1,256}\\.[a-zA-Z0-9()]{1,6}\\b(?:[-a-zA-Z0-9()@:%_\\+.~#?&\\/=]*)$",
      message = "The video url is not valid.")
  @Schema(
      description =
          "The url to a promotional video for the job offer that is being embedded on the site",
      example = "https://www.youtube.com/watch?v=dQw4w9WgXcQ")
  private String videoUrl;

  @Valid
  @Schema(description = "The address of the job offer")
  private AddressDto address;

  @NotNull(message = "The job offer must have at least one qualification")
  @Schema(description = "The qualifications needed for the job offer")
  private Set<String> qualifications;

  @NotNull(message = "The job offer may not be null.")
  @Schema(description = "The classification needed for the job offer")
  private String classification;

  @Valid
  @NotNull(message = "The job offer may have at least one language level.")
  @Schema(description = "The language levels needed for the job offer")
  private Set<LanguageLevelDto> languageLevels;
}
