package com.jobaidukraine.core.adapter.in.rest.controller;

import com.jobaidukraine.core.adapter.in.rest.assemblers.CompanyResourceAssembler;
import com.jobaidukraine.core.adapter.in.rest.dto.CompanyDto;
import com.jobaidukraine.core.adapter.in.rest.mapper.CompanyMapper;
import com.jobaidukraine.core.domain.Company;
import com.jobaidukraine.core.services.ports.in.queries.CompanyQuery;
import com.jobaidukraine.core.services.ports.in.usecases.CompanyUseCase;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import io.swagger.v3.oas.annotations.media.ArraySchema;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1.0/companies")
@RequiredArgsConstructor
@OpenAPIDefinition(
    info =
        @Info(
            title = "JobAidUkraine",
            version = "1.0",
            description = "JobAidUkraine REST API",
            license =
                @License(name = "MIT", url = "https://gitlab.com/jobaid/base/-/blob/main/LICENSE")),
    tags = {@Tag(name = "Company")})
public class CompanyController {
  private final CompanyUseCase companyUseCase;
  private final CompanyQuery companyQuery;
  private final PagedResourcesAssembler<CompanyDto> pagedResourcesAssembler;
  private final CompanyResourceAssembler companyResourceAssembler;
  private final CompanyMapper companyMapper;

  @Operation(
      summary = "Create new company",
      tags = {"Company"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful creation of company",
            content = {
              @Content(
                  mediaType = "application/json",
                  schema = @Schema(implementation = CompanyDto.class))
            }),
        @ApiResponse(responseCode = "400", description = "Invalid company data"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
      })
  @PostMapping
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
  public EntityModel<CompanyDto> save(@RequestBody CompanyDto company) {
    company =
        companyMapper.domainToDto(this.companyUseCase.save(companyMapper.dtoToDomain(company)));
    return companyResourceAssembler.toModel(company);
  }

  @Operation(
      summary = "Get company by id",
      tags = {"Company"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful retrieval of all companies",
            content = {
              @Content(
                  mediaType = "application/json",
                  array = @ArraySchema(schema = @Schema(implementation = CompanyDto.class)))
            }),
        @ApiResponse(responseCode = "400", description = "Invalid pageable supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "404", description = "No company found"),
      })
  @GetMapping
  public PagedModel<EntityModel<CompanyDto>> list(Pageable pageable) {
    Page<CompanyDto> companies =
        this.companyQuery.findAllByPageable(pageable).map(companyMapper::domainToDto);
    return pagedResourcesAssembler.toModel(companies, companyResourceAssembler);
  }

  @Operation(
      summary = "Get company by id",
      tags = {"Company"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful retrieval of company",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = CompanyDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "404", description = "Company not found"),
      })
  @GetMapping(value = "/{id}")
  public EntityModel<CompanyDto> show(@PathVariable long id) {
    Company company = this.companyQuery.findById(id);
    return companyResourceAssembler.toModel(companyMapper.domainToDto(company));
  }

  @Operation(
      summary = "Update company by id",
      tags = {"Company"})
  @ApiResponses(
      value = {
        @ApiResponse(
            responseCode = "200",
            description = "Successful update of company",
            content =
                @Content(
                    mediaType = "application/json",
                    schema = @Schema(implementation = CompanyDto.class))),
        @ApiResponse(responseCode = "400", description = "Invalid id or company supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
      })
  @PatchMapping(value = "/{id}")
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
  public EntityModel<CompanyDto> update(@RequestBody CompanyDto company, @PathVariable long id) {
    company.setId(id);
    company =
        companyMapper.domainToDto(this.companyUseCase.update(companyMapper.dtoToDomain(company)));

    return companyResourceAssembler.toModel(company);
  }

  @Operation(
      summary = "Delete company by id",
      tags = {"Company"})
  @ApiResponses(
      value = {
        @ApiResponse(responseCode = "200", description = "Successful deletion of company"),
        @ApiResponse(responseCode = "400", description = "Invalid id supplied"),
        @ApiResponse(responseCode = "401", description = "Unauthorized"),
        @ApiResponse(responseCode = "404", description = "Company not found"),
      })
  @DeleteMapping(value = "/{id}")
  @PreAuthorize(
      "hasAuthority('ROLE_ADMIN') or hasAuthority('ROLE_EDITOR') or hasAuthority('ROLE_USER')")
  @ResponseStatus(HttpStatus.NO_CONTENT)
  public void delete(@PathVariable long id) {
    this.companyUseCase.delete(id);
  }
}
