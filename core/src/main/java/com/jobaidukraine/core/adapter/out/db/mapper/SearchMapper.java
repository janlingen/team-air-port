package com.jobaidukraine.core.adapter.out.db.mapper;

import com.jobaidukraine.core.adapter.out.db.entities.LanguageLevelEntity;
import com.jobaidukraine.core.adapter.out.db.entities.SearchEntity;
import com.jobaidukraine.core.domain.LanguageLevel;
import java.util.Set;

import com.jobaidukraine.core.domain.Search;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
        componentModel = "spring",
        implementationName = "OutDbSearchMapperImpl",
        injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface SearchMapper {
    Search toDomain(SearchEntity searchEntity);

    SearchEntity toEntity(Search search);

    Set<Search> toDomainSet(Set<SearchEntity> searchEntities);

    Set<SearchEntity> toEntitySet(Set<Search> searches);
}