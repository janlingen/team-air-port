package com.jobaidukraine.core.adapter.out.emailsender;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.MailException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

@Service
public class EmailSenderService {

  @Autowired private JavaMailSender mailSender;

  /**
   * Return false on email send error
   * @param targetEmail
   * @param subject
   * @param body
   * @return
   */
  public boolean sendMail(String targetEmail, String subject, String body) {
    SimpleMailMessage message = new SimpleMailMessage();
    message.setFrom("hda@jobaidukraine.com");
    message.setTo(targetEmail);
    message.setSubject(subject);
    message.setText(body);

    try {
      mailSender.send(message);
    } catch (MailException ex) {
      return false;
    }
    return true;
  }
}
