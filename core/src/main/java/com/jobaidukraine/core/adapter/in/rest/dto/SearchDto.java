package com.jobaidukraine.core.adapter.in.rest.dto;

import java.util.List;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchDto {
  private Float latitude;
  private Float longitude;
  private Float radius;
  private String buzzwords;
}
