package com.jobaidukraine.core.adapter.in.rest.mapper;

import com.jobaidukraine.core.adapter.in.rest.dto.HousingDto;
import com.jobaidukraine.core.domain.Housing;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;

@Mapper(
    componentModel = "spring",
    implementationName = "InRestHousingMapperImpl",
    injectionStrategy = InjectionStrategy.CONSTRUCTOR,
    uses = {AddressMapper.class})
public interface HousingMapper {

  Housing dtoToDomain(HousingDto housingDto);

  HousingDto domainToDto(Housing housing);
}
