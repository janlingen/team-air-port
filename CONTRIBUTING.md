# Contributing

## Creating a new service
Please follow the rules when creating a new service

1. All services must contain a readme.md
1. For the readme, use the (to be defined) readme structure template
1. If a documentation, besides the code, is needed, put it directly into `md` or `adoc` files. If an external documentation reference is needed, put it into the readme.
1. Using a code linter is obligatory
1. Always provide a Dockerfile from the beginning of the service
1. Write tests when needed
1. Use OAS3.0 for API documentation
