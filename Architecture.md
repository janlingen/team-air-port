# Architecture proposal

## Last changed
03/12/2022

## General information
JobAid is a platform that has the goal to match refugees who are looking for jobs with companies that offer jobs especially to refugees. 
The platforms source code is open source and relies on an active community. 
This implies an always changing team.
The sociotechnical challenge is reflected to our architecture and code. 

## Status quo
Right now the whole systems logic is developed in a minimal customized wordpress installation.
The problems are crucial:
1. Lack of scalability: The system is hosted in a dockerized environment. A database keeps all information in place, while blob files are stored directly on to the hosts disc via a host binding. This setup does not allow horizontal scaling over more nodes.
1. Lack of integration: Indeed, wordpress offers a rudimental  REST-API that works well for a CMS. With more integration and collaboration partners to come, a strong integration layer, represented by a API that allows granular access management, is obligatory. 
1. Lack of plugin maturity: The core value of this project is the WP job plugin. This plugin has some bugs and therefore is not mature enough to rely on from a core value perspective. 
1. Lack of technical acceptance: Wordpress is a number one CMS but many developers refuse to code wordpress plugins. Special skills are needed to guarantee a rock solid operation of service.

## Operational target
The project might grow or shrink in its visitor count and therefore in its utilization. Regarding the ISO/IEC 25010 quality attributes, the platform must ensure a very high availability achieved by horizontal scalability. 
Due to the fact that JobAid is an early phase NGO, monetary resources are spare. 
A Kubernetes (k8s) cluster would be the perfect environment to be operated in from a technical perspective. 
A k8s cluster relies on a considerable amount of maintenance which can not be guaranteed. Hence a PaaS approach of proprietary services like AWS ElasticBeanstalk is used to the fact it offers nearly no operational maintenance and automatic scaling. 

## Service architecture
### Requirements
JobAid is changing at a very high pace, so changes must be made fast under a high quality standard. 

### Core service
The core service is a self contained service which offers the core functionality of the platform. This contains functions like CRUD operations on jobs and user profiles. 

### GEO suggestion service
The GEO Service is a microservice that handles all geo suggestion functionality.
Functions are 
1. Fetching (town, country) --> (lat, lng) data from the Google Places API
1. Transform retrieved data
1. Cache data from point 1
1. Return town, country, lat, lng tuples for queried data

### Matching service (Definition WIP)
<u>TO BE DISCUSSED:</u> The matching service matches applicants with job offerings. This service might be a part of the core service for performance reasons.
